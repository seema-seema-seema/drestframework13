from django.contrib import admin
from django.urls import path, include
from firstapp import views


urlpatterns = [

    path('', include('firstapp.urls')),
    # eh addkita NOTE : eh ek line likhn nal login vala show hon lag jana webpage te + settings.py ch DEFAULT_PERMISSION_CLASSES ede ch jana
    path('api/', include('rest_framework.urls')),
    path('admin/', admin.site.urls),
]
